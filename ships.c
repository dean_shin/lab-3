#include <stdlib.h>
#include <stdio.h>
#include "ships.h"
#include "ship.h"

struct Ship*** createShips(int gridSize) {
	struct Ship*** ptr;
	ptr = (struct Ship***) malloc(sizeof(struct Ship**) * gridSize);
	for(int i = 0; i < gridSize; i++) {
		ptr[i] = (struct Ship**) malloc(sizeof(struct Ship*) * gridSize);
		for(int j = 0; j < gridSize; j++) {
			ptr[i][j] = NULL;
		}
	}
	return ptr;
}

void deallocateShips(struct Ship*** ships, int gridSize) {
	for(int i = 0; i < gridSize; i++) {
		for(int j = 0; j < gridSize; j++) {
			if(ships[i][j] != NULL) {
				removeShip(ships, ships[i][j]);
			}
		}
	}
	for(int i = 0; i < gridSize; i++) {
		free(ships[i]);
	}
	free(ships);
}

void printShips(struct Ship*** ships, int gridSize) {
	printf("   ");
	for(int i = 0; i < gridSize; i++) {
		printf("%c ", 'a' + i);
	}
	printf("\n");
	printf("  +");
	for(int i = 0; i < gridSize; i++) {
		printf("__");
	}
	printf("\n");
	for(int i = 0; i < gridSize; i++) {
		if(i + 1 < 10) {
			printf("%i |", i + 1);
		} else {
			printf("%i|", i + 1);
		}
		for(int j = 0; j < gridSize; j++) {
			if(ships[i][j] == NULL) {
				printf("- ");
			} else {
				printf("%c ", ships[i][j]->alias);
			}
		}
		printf("\n");
	}
}

struct Ship* createShip(char alias, int startingRow, int startingCol, int isVertical) {
	struct Ship* ship = (struct Ship*) malloc(sizeof(struct Ship));
	ship->alias = alias;
	ship->startingRow = startingRow;
	ship->startingCol = startingCol;
	ship->isVertical = isVertical;
	ship->hits = 0;
	if(alias == 'c') {
		ship->length = 5;
	} else if(alias == 'b') {
		ship->length = 4;
	} else if (alias == 'f') {
		ship->length = 2;
	} else {
		printf("Error! Cannot create ship of unknown type: %c.\n", alias);
		exit(-1);
	}
	return ship;
}

bool addShip(struct Ship*** ships, int gridSize, struct Ship* ship) {
	// check if there is space
	for(int k = 0; k < ship->length; k++) {
		if(ship->isVertical) {
			if(ship->startingRow + k >= gridSize || (ships[ship->startingRow + k][ship->startingCol] != NULL)) {
				return false;
			}
		} else {
			if(ship->startingCol + k >= gridSize || (ships[ship->startingRow][ship->startingCol + k] != NULL)) {
				return false;
			}
		}
	}
	// there is space, so add.
	for(int k = 0; k < ship->length; k++) {
		if(ship->isVertical) {
			ships[ship->startingRow + k][ship->startingCol] = ship;
		} else {
			ships[ship->startingRow][ship->startingCol + k] = ship;
		}
	}
	return true;
}

void removeShip(struct Ship*** ships, struct Ship* ship) {
	for(int k = 0; k < ship->length; k++) {
		if(ship->isVertical) {
			ships[ship->startingRow + k][ship->startingCol] = NULL;
		} else {
			ships[ship->startingRow][ship->startingCol + k] = NULL;
		}
	}
	free(ship);
}