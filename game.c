#include <stdlib.h>
#include <stdio.h>
#include "game.h"
#include "ship.h"


char** createGame(int gridSize) {
	char** game;
	game = (char**) malloc(sizeof(char*) * gridSize);
	for(int i = 0; i < gridSize; i++) {
		game[i] = (char*) malloc(sizeof(char) * gridSize);
		for(int j = 0; j < gridSize; j++) {
			game[i][j] = '-';
		}
	}
	return game;
}

void deallocateGame(char** game, int gridSize) {
	for(int i = 0; i < gridSize; i++) {	
		free(game[i]);
	}
	free(game);
}

void printGame(char** game, int gridSize) {
	printf("   ");
	for(int i = 0; i < gridSize; i++) {
		printf("%c ", 'a' + i);
	}
	printf("\n");
	printf("  +");
	for(int i = 0; i < gridSize; i++) {
		printf("__");
	}
	printf("\n");
	for(int i = 0; i < gridSize; i++) {
		if(i + 1 < 10) {
			printf("%i |", i + 1);
		} else {
			printf("%i|", i + 1);
		}
		for(int j = 0; j < gridSize; j++) {
			printf("%c ", game[i][j]);
		}
		printf("\n");
	}
}

int attackPosition(char** game, struct Ship*** ships, int gridSize, int row, int col, int* shipsSunk) {
	// Don't allow the user to hit a spot out of bounds.
	if(row < 0 || row >= gridSize || col < 0 || col >= gridSize) {
		return 0;
	}
	// Don't allow the user to hit a user to hit a spot that they already hit.
	if(game[row][col] != '-') {
		return 0;
	}
	// If there are no ships at the row and col, it is a miss.
	if(ships[row][col] == NULL) {
		game[row][col] = 'm';
		return 2;
	}
	// Otherwise, it is a hit.
	struct Ship* ship = ships[row][col];
	game[row][col] = 'h';
	ship->hits++;
	// If over 70% of the ship is destroyed, destroy the entire ship.
	if(((double) ship->hits) / ((double) ship->length) > 0.70) {
		for(int k = 0; k < ship->length; k++) {
			if(ship->isVertical && game[ship->startingRow + k][ship->startingCol] == '-') {
				game[ship->startingRow + k][ship->startingCol] = ship->alias;
			}
			if(!ship->isVertical && game[ship->startingRow][ship->startingCol + k] == '-') {
				game[ship->startingRow][ship->startingCol + k] = ship->alias;
			}
		}
		*shipsSunk += 1;
	}
	return 1;
}