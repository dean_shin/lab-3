#pragma once
#include <stdbool.h>

struct Ship {
    char alias;
    int startingRow;
    int startingCol;
    int length;
    int isVertical;
    int hits;
};