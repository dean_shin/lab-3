# Building the Program

Enter the following command:
`make`

# Running the Program

Enter the following command to run the program in regular mode:
`./frigate`

Enter the following command to run the program with a demo file:
`./frigate <input_file>`