#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include "ship.h"
#include "game.h"
#include "ships.h"

bool decodePosition(char* encoded, int* row, int* col);
void populateShips(struct Ship*** ships, int gridSize, char* inputFile);
void populateShipsRandom(struct Ship*** ships, int gridSize);

int main(int argc, char *argv[ ]) {
	int gamesWon = 0;
	int games = 0;
	srand(time(0));

	while(true) {
		games++;
		printf("How large should I make the grid? ");
		
		// initialize gridSize //
		char gridSizeBuffer[100];
		int gridSize;
		while(fgets(gridSizeBuffer, 100, stdin) == NULL || 
			sscanf(gridSizeBuffer, "%i", &gridSize) != 1) {
			printf("Invalid grid size. Please enter an integer: ");
		}
		printf("\n");

		if(gridSize < 5) {
			printf("The minimum grid size is 5. I'll create one that size.\n");
			gridSize = 5;
		}
		if(gridSize > 20) {
			printf("The maximum grid size is 20. I'll create one that size.\n");
			gridSize = 20;
		}
		
		char** game = createGame(gridSize);
		struct Ship*** ships = createShips(gridSize);
		
		// Initialize ship positions. //
		if(argc > 2) {
			printf("Received invalid number of program parameters. Received %i, but expected 1 or 2.\n", argc);
			exit(-1);
		} 
		// Use demo ship positions //
		else if(argc == 2) {
			printf("Reading ship positions from demo file %s.\n\n", argv[1]);
			populateShips(ships, gridSize, argv[1]);
		} 
		// Use random ship positions //
		else {
			printf("Generating random ship positions...\n\n");
			populateShipsRandom(ships, gridSize);
		}

		// printShips(ships, gridSize); // For debug!
		printGame(game, gridSize);
		printf("\n");
		
		int numShells = gridSize * gridSize / 2;
		int shipsSunk = 0;
		while(numShells > 0) {
			// process attack //
			while(true) {
				printf("Enter the coordinate for your shot (%i shots remaining): ", numShells);
				int row;
				int col;
				char encodedPosition[100];
				fgets(encodedPosition, 100, stdin);
				printf("\n");

				bool positionValid = decodePosition(encodedPosition, &row, &col);
				if(!positionValid) {
					printf("Invalid position inputted. Please try again!\n\n");
					continue;
				}

				int attackStatus = attackPosition(game, ships, gridSize, row, col, &shipsSunk);
				if(attackStatus == 0) {
					printf("Invalid attack coordinates. Please try again!\n\n");
					continue;
				} else if (attackStatus == 1) {
					printf("%c%i is a hit!\n\n", 'a' + col, row + 1);
					break;
				} else {
					printf("%c%i is a miss...\n\n", 'a' + col, row + 1);
					break;
				}
			}
			
			// print out game board //
			printGame(game, gridSize);
			printf("\n");

			// exit early if all ships have been sunk //
			if(shipsSunk == 4) {
				break;
			}
			numShells--;
		}
		
		// win logic //
		if(shipsSunk == 4) {
			printf("Congratulations! You sunk all 4 ships!\n");
			printf("Here are the original ship locations.\n\n");
			printShips(ships, gridSize);
			printf("\n");
			gamesWon++;
		} 
		// lose logic //
		else {
			printf("You do not have enough shells left to sink the remaining ships.\n");
			printf("Here are the original ship locations.\n");
			printShips(ships, gridSize);
			printf("\n");
			printf("You sunk %i ships.\n", shipsSunk);
		}

		deallocateGame(game, gridSize);
		deallocateShips(ships, gridSize);
		
		char playAgain[5];
		printf("Play again? y/n: ");
		fgets(playAgain, 5, stdin);
		if(playAgain[0] == 'n') {
			break;
		}
	}
	printf("You won %i/%i games.\n", gamesWon, games);
	printf("Thanks for playing!\n");

	return 0;
}

void populateShips(struct Ship*** ships, int gridSize, char* inputFilePath) {
	FILE *inputFile = fopen(inputFilePath, "r");

	if(inputFile == NULL) {
		printf("Error! Could not open file.\n");
		exit(-1);
	}

	char line[100];
	while(fgets(line, 100, inputFile) != NULL) {
		if(line[0] == '#') {
			// Ignore, comment line.
			continue;
		}
		char alias;
		char orientation;
		char encodedPosition[100];
		int isVertical = 0;
		int startingRow;
		int startingCol;

		sscanf(line, "%c %c %s", &alias, &orientation, encodedPosition);

		if(orientation == 'r') {
			isVertical = true;
		} else if (orientation == 'c') {
			isVertical = false;
		} else {
			printf("Unknown ship orientation: %c. Defaulting to horizontal.\n", orientation);
			isVertical = false;
		}
		decodePosition(encodedPosition, &startingRow, &startingCol);

		struct Ship* toAdd = createShip(alias, startingRow, startingCol, isVertical);

		if(!addShip(ships, gridSize, toAdd)) {
			// Deallocate memory allocated to ship.
			free(toAdd);
			printf("Error! Could not add ship as it either went out of bounds or it collided with another ship! Try increasing the grid size.\n");
			exit(-1);
		}
		
	}

	fclose(inputFile);
}

void populateShipsRandom(struct Ship*** ships, int gridSize) {
	char* aliases = "cbff";
	for(int i = 0; i < 4; i++) {
		while(true) {
			char alias = aliases[i];
			int isVertical = rand() % 2 == 0;
			int startingRow = rand() % gridSize;
			int startingCol = rand() % gridSize;
			
			struct Ship* toAdd = createShip(alias, startingRow, startingCol, isVertical);
			// If we were successful in adding the ship, add the next one.
			// Otherwise, randomly generate another ship.
			if(addShip(ships, gridSize, toAdd)) {
				break;
			} else {
				// Deallocate memory allocated to ship.
				free(toAdd);
			}
		}
	}
}

// encoded will either be in the form of character-digit, character-digit-digit, digit-character, or digit-character-character.
// Returns true if decode was successful, false if input was invalid.
bool decodePosition(char* encoded, int* row, int* col) {
	char alpha;
	char digit1;
	char digit2;
	// letter-digit
	if(isalpha(encoded[0]) && isdigit(encoded[1]) && (encoded[2] == '\n' || encoded[2] == '\0')) {
		alpha = encoded[0];
		digit1 = '0';
		digit2 = encoded[1];
	} 
	// letter-digit-digit
	else if (isalpha(encoded[0]) && isdigit(encoded[1]) && isdigit(encoded[2]) && (encoded[3] == '\n' || encoded[3] == '\0')) {
		alpha = encoded[0];
		digit1 = encoded[1];
		digit2 = encoded[2];
	}
	// digit-letter
	else if (isdigit(encoded[0]) && isalpha(encoded[1]) && (encoded[2] == '\n' || encoded[2] == '\0')) {
		alpha = encoded[1];
		digit1 = '0';
		digit2 = encoded[0];
	}
	// digit-digit-letter
	else if (isdigit(encoded[0]) && isdigit(encoded[1]) && isalpha(encoded[2]) && (encoded[3] == '\n' || encoded[3] == '\0')) {
		alpha = encoded[2];
		digit1 = encoded[0];
		digit2 = encoded[1];
	}
	// none--invalid input. 
	else {
		return false;
	}

	if(isupper(alpha)) {
		*col = alpha - 'A';
	} else {
		*col = alpha - 'a';
	}
	*row = 10 * (digit1 - '0') + (digit2 - '0') - 1;
	return true;
}
