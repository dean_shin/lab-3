#pragma once
#include "ship.h"

struct Ship*** createShips(int gridSize);
void deallocateShips(struct Ship*** ships, int gridSize);
void printShips(struct Ship*** ships, int gridSize);
struct Ship* createShip(char alias, int startingRow, int startingCol, int isVertical);
// Try adding ship to the ships matrix. Returns true if addition was successful, and false if addition was unsuccessful.
bool addShip(struct Ship*** ships, int gridSize, struct Ship* ship);
// Remove ship from the ships matrix. Also, free the space allocated to Ship.
void removeShip(struct Ship*** ships, struct Ship* ship);