#pragma once
#include "ship.h"

char** createGame(int gridSize);
void deallocateGame(char** game, int gridSize);
void printGame(char** game, int gridSize);
// Try attacking a position on the board. Returns 0 if the attack was invalid, 1 if the attack hit a target, and 2 if the attack missed.
int attackPosition(char** game, struct Ship*** ships, int gridSize, int row, int col, int* shipsSunk);